

var Logger = require('./Logger');
var DateUtil = require('./DateUtil');

module.exports = function(){

    var isRunning = false;
    var prevData = Logger.getLocalLogs();
    var today = DateUtil.formatDate(new Date());
    console.log('today', today);
    var prevDataForToday = (function(){
        try{
            return prevData[today];
        } catch(e){
            return null;
        }
    })();

    var consumptionToday = prevDataForToday ? prevDataForToday['energy_used_today'].value : 0;
    var consumptionPrevDays = 0;
    var dailyAVG = 0;

    var consumptionTracker = DailyConsumptionTracker(consumptionToday);

    try{
        consumptionPrevDays = Object.keys(prevData)
            .filter(function(k){  return k !== today })
            .map(function(k){  return prevData[k]['energy_used_today'].value })
            .reduce(function(a, b){  return a+b  });

        dailyAVG = consumptionPrevDays/Object.keys(prevData).length;
        console.log("dailyAVG:",  dailyAVG);
        consumptionPrevDays = dailyAVG*77;
    } catch(e){
        console.log(e);
    }
    
    var timer = null;
    var interval = 1000;

    var AVG_CONSUMPTION = 25.33;


    var onDataCallback = null;


    return {
        
        consumptionTracker: consumptionTracker,
        CO2Calculator: CO2Calculator,
        
        isRunning: function(){
            return isRunning;
        },

        setInterval: function(_interval){
            interval = _interval;
            return this;
        },

        onData: function(callback){
            onDataCallback = callback;
        },


        start: function(){
            console.log('starting localDataProvider');
            timer = setInterval(function(){

                //today
                var consumptionToday = consumptionTracker.getConsumption(AVG_CONSUMPTION, interval);
                var co2Today = CO2Calculator.getCO2(consumptionToday);

                //all since beginning
                var totalConsumption = consumptionPrevDays + consumptionToday;
                var totalCO2 = CO2Calculator.getCO2(totalConsumption);

                onDataCallback( {
                    'energy_used_today': { unit: "wh", value: consumptionToday  }, // mult by 1000 to return wh
                    'co2_emitted_today': { unit: 'g',  value: co2Today          },
                    'energy_used_total': { unit: "wh", value: totalConsumption  },
                    'co2_emitted_total': { unit: 'g',  value: totalCO2           }
                });
            }, interval);
            isRunning = true;
            return this;
        },

        stop: function(){
            clearInterval(timer);
            isRunning = false;
        },
        
        dataEventName: 'localStats'

    }

};


var DailyConsumptionTracker = function (prevConsumption) {
    var lastPower = 0;
    // total energy consumption in wh
    var consumption =  prevConsumption;

    return {
        getConsumption: function(power, interval){
            return (consumption += ( ( power+lastPower ) / 2) * interval / 3600000);
        }
    }

}


var CO2Calculator = (function(){

    // carbon intensity of source powerplant in kg / kWh
    var ppCarbonIntensity = 0.376;
    // 0.376 tons per mWh =
    // 0.000376 tons per kWh =
    // 0.376 kg's per kWh =
    // 376g / kWh =
    // 0.376g / Wh

    return {
        getCO2: function(consumptionWH){
            return (consumptionWH * ppCarbonIntensity);
        }
    }
})();