/**
 * Created by GaborK on 10/04/16.
 */



module.exports = (function(){
    return {
        "formatDate": function(dt){
            return dt.getDate()+"-"+dt.getMonth()+"-"+dt.getFullYear();

        }
    }
})();
