


module.exports = function(){

    var startVal = 0;
    var startTime = null;
    return {

        start: function(val){
            console.log('startval', val);
            startVal = val;
            startTime = new Date().getTime();
            return this;
        },

        end: function(endval){
            var endTime = new Date().getTime();
            console.log('change of val', endval - startVal);
            console.log('time elapsed', endTime - startTime);
            return (endval - startVal) / (endTime - startTime);
        }
    }

};