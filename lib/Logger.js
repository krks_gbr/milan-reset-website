
var fs = require('fs');
var DateUtil = require('./DateUtil');

module.exports =  (function Logger(){
    
    var localJSONPath = 'logs/local.json';
    var globalJSONPath = 'logs/global.json';

    return {
        'logLocal': function(data){
            var entry = {};

            try{
                entry = readJSON(localJSONPath);
            } catch(e){
                console.log("Logger.logLocal:16", e);
            }

            var today = DateUtil.formatDate(new Date());
            entry[today] = data;
            writeJSON(localJSONPath, entry);
        },

        'logGlobal': function(data){
            
        },
        
        "getLocalLogs": function(){
            try{
                return readJSON(localJSONPath);
            } catch(e){
                return null;
            }
        }
    }
    
})();

function readJSON(path){
    return JSON.parse(fs.readFileSync(path));
}

function writeJSON(path, data){
    fs.writeFileSync(path, JSON.stringify(data, null, 4));
}
