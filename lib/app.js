
//var scraperAddress = process.env.NODE_ENV == 'production' ? 'http://phantom.krks.info:5555' : 'http://localhost:5555';
var scraperAddress = "http://phantom.krks.info:5555";
var socketClient = require('socket.io-client')(scraperAddress);

var Logger = require('./Logger');
var localDataProvider = require('./LocalDataProvider')();
var updateInterval = 100;

module.exports = function(io){


    // here should implement timeout, and fallback mechanism
    var globalDataProviderCreated = new Promise(function(resolve, reject){
        console.log('waiting for globalDataProvider...');
        socketClient.on('globalStats', function(data){
            resolve(require('./GlobalDataProvider')(data));
        });
    });


    socketClient.on('connect', function () {
        console.log('connected to scraper at', scraperAddress);
        socketClient.emit('message', { 'sender': 'pi_backend'});
    });



    io.on('connection', function (socket) {
        
        if(!localDataProvider.isRunning()){
            localDataProvider.setInterval(updateInterval).start();
        }

        var dataCopy = null;
        localDataProvider.onData(function(result){
            socket.emit('localStats', result);
            dataCopy = JSON.parse(JSON.stringify(result));
            delete dataCopy['energy_used_total'];
            delete dataCopy['co2_emitted_total'];

        });

        var logTimer = setInterval(function(){

            if(dataCopy){
                Logger.logLocal(dataCopy);
            }

        }, 10000);

        
        globalDataProviderCreated.then(function(globalDataProvider){
            if(!globalDataProvider.isRunning()){
                globalDataProvider.setInterval(updateInterval).start();
            }
            globalDataProvider.onData(function(result){
                socket.emit('globalStats', result);
            });
        });

        
        socket.on('message', function(data){
            console.log('message received from', data['sender']);
        });
        
        socket.on('disconnect', function(){
            if(io.engine.clientsCount == 0){
                console.log('no more clients');
                localDataProvider.stop();
                globalDataProviderCreated.then(function(globalDataProvider){
                    globalDataProvider.stop();
                });
                clearInterval(logTimer);
            } 
        });

    });
}

