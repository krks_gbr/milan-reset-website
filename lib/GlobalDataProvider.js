/**
 * Created by GaborK on 11/04/16.
 */




var Logger = require('./Logger');
var DateUtil = require('./DateUtil');

module.exports = function(initialValues){
    
    console.log('created globalDataProiveder');
    
    var isRunning = false;
    var localData = Logger.getLocalLogs();
    var days = Object.keys(localData).filter(function (key) {
        return key != DateUtil.formatDate(new Date());
    }).length;



    var co2Today = initialValues['co2_emitted_today'].value;
    var co2EmissionRate = initialValues['co2_emitted_today'].rate;
    var consumptionToday = initialValues['energy_used_today'].value;
    var consumptionRate = initialValues['energy_used_today'].rate;

    var consumptionPrevDays = days*24*60*60000 * consumptionRate;
    var co2EmissionPrevDays = days*24*60*60000 * co2EmissionRate;

    var carbonBudgetUsedUp = 0.8;



    var timer = null;
    var interval = 1000;
    var onDataCallback = null;


    return {

        isRunning: function(){
            return isRunning;
        },

        setInterval: function(_interval){
            interval = _interval;
            return this;
        },

        onData: function(callback){
            onDataCallback = callback;
        },


        start: function(){
            console.log('starting globalDataProvider');

            timer = setInterval(function(){
                co2Today += co2EmissionRate*interval;
                consumptionToday += consumptionRate*interval;

                var totalCO2 = co2Today + co2EmissionPrevDays;
                var totalConsumption = consumptionToday + consumptionPrevDays;

                onDataCallback( {
                    'energy_used_today': { unit: "mwh", value: consumptionToday  },
                    'co2_emitted_today': { unit: 'tons',  value: co2Today        },
                    'energy_used_total': { unit: "mwh", value: totalConsumption  },
                    'co2_emitted_total': { unit: 'tons',  value: totalCO2        },
                    'carbon_budget_used': { unit: 'part', value: carbonBudgetUsedUp },
                });
            }, interval);

            isRunning = true;
            return this;
        },

        stop: function(){
            console.log('stopping globalDataProvider');
            clearInterval(timer);
            isRunning = false;
        },
        
        dataEventName: 'localStats'
    }

};