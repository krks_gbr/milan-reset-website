
var chp = require('child_process');
var server = chp.fork('./server');

console.log(process.env.NODE_ENV);

function startBrowser(){
    var browser;
    if(process.env.NODE_ENV == 'production'){
        console.log('opening iceweasel');
        browser = chp.exec('MOZILLA_DISABLE_PLUGINS=1 iceweasel -fullscreen -url http://localhost:3000');



        setTimeout(function(){
            console.log("refreshing iceweasel");
            chp.exec('xdotool key --window "$(xdotool search --name running)" F5');

        }, 5000);



        // setTimeout(function(){
        //     console.log("making iceweasel fullscreen");
        //     // makes browser full screen
        //     chp.exec('xdotool key --window "$(xdotool search --name running)" F11');
        // }, 20000);

    } else {
        browser = chp.exec('/Applications/Firefox.app/Contents/MacOS/firefox -url http://localhost:3000')
    }

    browser.stdout.on('data', function(data)  {
        console.log("stdout:", data);
        logError("stdout: " + data);
    });

    browser.stderr.on('data', function(data)  {
        console.log("stderr:", data);
        logError("stderr: " + data);
    });

    browser.on('close', function(code)  {
        console.log("Firefox exited", code);
        logError('Browser exited with code ' + code);
       // startBrowser();
    });

    return browser;
}

startBrowser();



var fs = require('fs');

function logError(err) {

    var data = '******************\n' + new Date().toISOString() + '\n' + err +'\n\n\n';
    fs.appendFile('exit-log', data, function(err){
        if(err){
            console.log(err);
        }
    });

}
