
var express = require('express');
var xpress = express();
var server = require('http').Server(xpress);
var io = require('socket.io')(server);

//initialize app
require('./lib/app')(io);

server.listen(3000, function(){
    console.log('app is listening on 3000');
});

 xpress.use(express.static(__dirname + '/front'));
//xpress.use(express.static(__dirname + '/datatest'));

xpress.get("/", function(req, res){
	res.sendFile('index.html');
});

// listen for TERM signal .e.g. kill
process.on ('SIGTERM', function(){
    console.log("received kill signal. terminating.");
    process.exit();
});

// listen for INT signal e.g. Ctrl-C
process.on ('SIGINT', function(){
    console.log("received kill signal. terminating.");
    process.exit();
});