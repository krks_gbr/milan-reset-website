/**
 * Created by GaborK on 09/04/16.
 */




var DataSource = (function(){

    var source = io.connect('http://localhost:3000');

    
    var events = [ 'localStats', 'globalStats'];

    source.on('connect', function(){
        console.log('connected to localSource');
        source.emit('message', { 'sender': 'pi_front' });
    });



    return {
        on: function(event, callback){
            if(events.indexOf(event) == -1){
                throw Error(event + " is not a valid event name");
                return;
            }
            source.on(event, callback);
        }
    }

})();



/*

 test/example

 */

var localHasPrinted = false;
DataSource.on('localStats', function(data){
    if(!localHasPrinted){
        console.log("localData", data);
        localHasPrinted = true;
    }
});

var globalHasPrinted = false;
DataSource.on('globalStats', function(data){
    if(!globalHasPrinted){
        console.log("globalData", data);
        globalHasPrinted = true;
    }
});