


//var address = "http://phantom.krks.info:5757";
var address = "http://localhost:5555";

var globalCO2EmissionsThisYear = null;

var socket = io.connect(address);
socket.on('emissions', function (data) {
    var emissions = data.filter(function(d){ return d.name == 'co2_emissions'  })[0].val;
    globalCO2EmissionsThisYear = parseInt(emissions.split(',').join(""));
});


// hourly consumption of clock
// based on
// http://raspberrypi.stackexchange.com/questions/5033/how-much-energy-does-the-raspberry-pi-consume-in-a-day

var clockConsumptionMW = 0.000004;

// tons of CO2 emmitted by powerplant / MWh
var powerplantCarbonIntensity = 0.376;


// global co2 consumptionRate / second in tons
// based on worldometer
var globalEmissionRate = 1200;


// how much carbon can we globally emit before reaching dangerous rates of temperature
var worldCarbonBudget = 565000000000;



var clockEmissionRate = (clockConsumptionMW / 3600) * (powerplantCarbonIntensity / 3600);

// co2 emitted by our clock
var emmittedByClock = 0;



// months left until we run out of carbon budget
var secondsLeft = -1;
var daysLeft = -1;



setInterval(updateValues, 100);


function updateValues(){
    worldCarbonBudget -= (globalEmissionRate/10);
    emmittedByClock += (clockEmissionRate/10);
    secondsLeft = (worldCarbonBudget / (globalEmissionRate/10)).toFixed(10);
    daysLeft = (secondsLeft/864000).toFixed(10);
    if(globalCO2EmissionsThisYear){
        setCounterValue('.globalCarbonEmission', numberWithCommas(globalCO2EmissionsThisYear));
    }
    setCounterValue('.clockEmission', numberWithCommas(emmittedByClock.toFixed(15)));
    setCounterValue('.carbonBudget', numberWithCommas(worldCarbonBudget));
    setCounterValue('.daysLeft', numberWithCommas(daysLeft));
}


function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}

function setCounterValue(_class, value){
    $(_class).find('.counterValue').text(value);
}


var sentences = [


    "We brag "+ "\n"+
    "of damage done"+ "\n"+
    "but whether we could truly"+ "\n"+
    "dry all rain, bake all earth,"+ "\n"+
    "science does not know.",


    "Together" + "\n"+
    "the weather" + "\n"+
    "is a language we can barely understand;" + "\n"+
    "but confessional experts detect" + "\n"+
    "in the senseless diktat of hurricane" + "\n"+
    "a hymning of our sins, our" + "\n"+
    "stupid counterpoint.",

    "We were the first" + "\n"+
    "cracking the hull of the earth" + "\n"+
    "open, our foolish husbandry" + "\n"+
    "a metallurgy that’s brimmed" + "\n"+
    "with false gold too often" + "\n"+
    "we can talk, and talk, and talk" + "\n"+
    "but a ship in space, manned" + "\n"+
    "by non-thinking from non-feeling," + "\n"+
    "says absolutely nothing at all."

    //"Denying climate change because it's cold is like denying world famine because you just had lunch.",
    //"Climate change is not a hoax. Droughts and floods and wildfires are not a joke -- they are a threat to our children's future.",
    //"We are the first generation to feel the effect of climate change and the last generation who can do something about it.",
    //"Climate change is a scientific issue. So why are people letting politics determine their position on it?",
    //"The united states must lead the world in reversing climate change and make certain that this planet is habitable for future generations.",
    //"Your house will be underwater in 45 years if it isn't placed on a hill.",
    //"There won't be enough food here anymore at 10% of the use of carbon budget.",
    //"The fan you put on now, gradually contributes to droughts, heatwaves and wildfires.",
    //"Your icecream would completely melt within 2 minutes 15 years from now.",
    //"Children of now will not always have enough energy during their lives."

];



for(var i = 0, si=1; i < sentences.length; i++, si+=2){
    var slide = $('<div>').attr('id', 'slide-' + si).addClass('hidden slide').appendTo('#wrapper');
    var container = $('<div>');
    container = container.addClass('sentence').text( sentences[ i ] );
    container.appendTo(slide);
}

var counters = $('.counter');
for(var j = 0, ji=2; j<counters.length; j++, ji+=2 ){
    var counterSlide = $('<div>').attr('id', 'slide-'+ji).addClass('hidden slide counter-slide').appendTo('#wrapper');
    var c = counters.get(j);
    $(c).clone().appendTo(counterSlide);
}

var currentSlideIndex = 0;

//
// setInterval(function(){
//     var currentSlide = $('#slide-' + currentSlideIndex);
//     currentSlide.fadeOut(500, function(){
//         currentSlideIndex = (currentSlideIndex+1) % $('.slide').length;
//
//         var nextSlide = $('#slide-'+currentSlideIndex);
//         nextSlide.fadeIn(500);
//     });
//
// }, 7500);





