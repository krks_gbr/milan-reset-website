/**
 * Created by GaborK on 30/03/16.
 */



var sentences = [
    "We brag of damage done",
    "Science does not know",
    "The weather is a language we can barely understand",
    "Confessional experts detect",
    "A hymning of our sins, our stupid counterpoint",
    "Cracking the hull of the earth",
    "We can talk, and talk, and talk...",
    "Suck on the black depths",
    "There must be somebody to blame...",
    "A planetary impersonal adjustment",
    "The wastefulness was all ours",
    "Global warming is a myth",
    "'What if I’d say global heating is caused by aliens'",
    "Tell me about how you're smarter than science",
    "Global heating isn’t real because it is cold now",
    "Cooling is the new warming",
    "Favourite crop: oil",
    "God’s curse for using fossil fuels ",
    "Block neutral forms of energy fills wallets",
    "Dinosaurs were made to fuel cars",
    "Stay for the black lung",
    "Welcome to the wasteland",
    "Global warming has become a new religion.",
];