// 
// 	VERA VAN DE SEYP | GABOR KEREKES | AMIR HOUIEH
//     ____  __  ___   ___   _______   ________   ____  __  ________
//    / __ \/ / / / | / / | / /  _/ | / / ____/  / __ \/ / / /_  __/
//   / /_/ / / / /  |/ /  |/ // //  |/ / / __   / / / / / / / / /   
//  / _, _/ /_/ / /|  / /|  // // /|  / /_/ /  / /_/ / /_/ / / /    
// /_/ |_|\____/_/ |_/_/ |_/___/_/ |_/\____/   \____/\____/ /_/     
//                                                                 
// 	RESET | SALONE DEL MOBILE | MILAN | 2K16  
//   




// 2777 KWh / year
// 4*2777 = 11108 KWh / year
// 30.4328767123 KWh / day

// 6*2777 = 16662 KWh / year
// 45.6493150685 KWh / day

// 1.90205479452 KWh 
// 1.90205479452  3600
// 0.00052834855 KW / ps




//parsers

// Converts decimals to ones with points
function convertToLegibleNumber() {
    $('.decimals').each(function () {
        var thing = $(this).text();
        $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
    });
}
// Converts days to years, months, days
function humaniseDays() {
    $('.times').each(function () {
        var number = parseInt($(this).text());
        var humanNumber = humanise(number);
        $(this).html(humanNumber);
    })
}
function humanise(diff) {
    var str = '';
    var values = [['<span> year</span>', 365], ['<span> month</span>', 30], ['<span> day</span>', 1]];

    for (var i = 0; i < values.length; i++) {
        var amount = Math.floor(diff / values[i][1]);

        if (amount >= 1) {
            str += amount + values[i][0] + (amount > 1 ? '<span>s</span>' : '') + ' ';
            diff -= amount * values[i][1];
        }
    }
    return str;
}
function numberFloat() {
    $('.number-float').each(function () {
        var decimalW = $(this).children('.decimals').width();
        var mesW = $(this).children('mes').width();
        var timesW = $(this).children('.times').width();

        $('this').css({
            'width': decimalW + mesW + timesW + 10
        })
    })
}


// initializes Containers with text
function initializeSentenceContainers() {
    var template = $('.items-sentence-template');
    var largeItems = $('.items').filter(function () {
        var large = $(this).find('.item-large')[0];
        if (large) {
            return true;
        }
        return false;
    });

    var template = $('.items-sentence-template');
    largeItems.each(function () {
        var sentenceItems = template.clone().removeClass('items-sentence-template').addClass('items sentence');
        sentenceItems.insertAfter($(this));
    });

    template.remove();
}
// reshuffles the content of the sentences
function reFillSentenceContainers() {
    sentences = sentences.shuffle();
    $('.items.sentence').each(function (index) {
        $(this).find('.text').text(sentences[index % sentences.length]);
    });

    $('.text').each( function() {
        if( $(this).text().length < 40 ) {
            $(this).css({
                'top' : $('.item').height(),
            })
        } else {
            $(this).css({
                'top' : '0',
            })
        }
    })
}
Array.prototype.shuffle = function () {
    var i = this.length, j, temp;
    if (i == 0) return this;
    while (--i) {
        j = Math.floor(Math.random() * ( i + 1 ));
        temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
    return this;
};

function insertDate() {

    var d = new Date();
    month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    $('.date').html('(' + day + ' – ' + month + ' – ' + year + ')' );
}


function setHeight() {

    var itemHeight = $('.rows').height() * 0.2;

    $('.item').css({
        'height': itemHeight,
    });

    $('.progress-bar').css({
        'height': itemHeight*0.2,
    });

    $('.explanation-large').css({
        'top' : itemHeight
    })

    $('.text').css({
        'padding-top' : itemHeight * .12,
        'font-size': itemHeight
    })

    $('.number-large').css({
        'font-size': itemHeight,
        'top' : itemHeight * 2,
    });

}

$(document).ready(function () {



    var exh_energy = 0;
    var exh_carbon = 0;


    var slides = [];
    var currentIdx = 0;
    var currentSlide = null;
    var animateSpeed = 1000;
    var timePerSlide = 10000;
    
    function _slideFrames(){

        var nextSlide;

        currentIdx = currentIdx>= slides.length-1? 0: currentIdx+1;
        nextSlide = $(slides[currentIdx]);

        currentSlide
            .removeClass('center')
            .addClass('left')

        nextSlide
            .removeClass('right')
            .addClass('center');

        setTimeout(function(){
            currentSlide
                .removeClass('left')
                .addClass('right');
            currentSlide = $(slides[currentIdx]);

        }, animateSpeed);

        if(exhibitionIdx.indexOf(currentIdx)!==-1){
            writeExhibitionEstimation();
            clearInterval( exhibitionTimer );

        }else{
            exhibitionTimer = setInterval(updateBuildingestimation, 1000);
        }

    }

    var exhibitionIdx = [];
    var exhibitionTimer = null;
    var slidesFields = {'global':[],'local':[]};
    var locals_name_template = {
        'local-energy':        "energy_used_today",
        'local-energy-total':  "energy_used_total",
        'local-carbon':        "co2_emitted_today",
        'local-carbon-total':  "co2_emitted_total"
    }
    var globals_name_template = {
        'global-energy':        'energy_used_today',
        'global-energy-total':  'energy_used_total',
        'global-carbon':        'co2_emitted_today',
        'global-carbon-total':  'co2_emitted_total'
    }
    function pushFields(){
        var local_fields = [];
        var global_fields = [];

        for(var className in locals_name_template){
            var field = $(this).find('.'+className);
            if(field.length) local_fields.push( {el:field, name:locals_name_template[className]});
        }

        for(var className in globals_name_template){
            var field = $(this).find('.'+className);
            if(field.length) global_fields.push( {el:field, name:globals_name_template[className]});
        }

        slidesFields.local.push( local_fields )
        slidesFields.global.push( global_fields );
    }
    function updateFields(source,_data){

        var currentSlideFields = slidesFields[source][currentIdx];
        if(!currentSlideFields) return;
        if(!currentSlideFields.length) return;

        for(var x=0; x<currentSlideFields.length; x++){
            var field = currentSlideFields[x];
            var fieldData = _data[ field.name ];

            var val = fieldData.value.toFixed(3);
            var str = val.toString();
            str = str.replace('.', ',');
            str = str.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
            
            // if(source == "global")
            //     val = ~~val;

            field.el.html( str+"0" + "<mes>" + fieldData.unit + '</mes>' );
        }
    }



    // var carbonRate = 0.376;
    // var increasePS = 0.00052834855;

    var carbonRate = 10.376;
    var increasePS = 10.00052834855;


    function initBuildingestimation() {
        var start = new Date();
        var startDate = start.getHours() + ':' + start.getMinutes() + ':' + start.getSeconds();
        var now = new Date();
        var time  = now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
        $('.exhibition-energy').html('<label class="value">0</label>' + ' <span>KW since ' +  startDate + '</span>' );
        $('.exhibition-carbon').html('<label class="value">0</label>' + ' <span>kg since ' +  startDate + '</span>' );

    }

    function writeExhibitionEstimation(){
        var energyField = $('.exhibition-energy').find('.value');
        var carbonField = $('.exhibition-carbon').find('.value');
        energyField.text(exh_energy.toFixed(3));
        carbonField.text(exh_carbon.toFixed(3));
    }

    function updateBuildingestimation(){
        exh_energy += increasePS;
        exh_carbon = exh_energy*carbonRate;
    }

    DataSource.on('localStats',function(_data){updateFields('local',_data)});
    DataSource.on('globalStats',function(_data){updateFields('global',_data)});




    $('.items').each(function(i){
        $(this).attr('index', i);
        slides.push(this);
        pushFields.call(this);
    })

    $('.exhibition-energy,.exhibition-carbon').each(function(){
        exhibitionIdx.push($(this).parents('.items').attr('index')*1);
    })

    currentSlide = $(slides[currentIdx]);

    setHeight();
    convertToLegibleNumber();
    humaniseDays();
    initializeSentenceContainers();
    reFillSentenceContainers();
    insertDate();
    initBuildingestimation();

    var loop = setInterval(_slideFrames, timePerSlide);
    var running = true;
    $('body').click(function(){
        if(running){
            clearInterval(loop);
        } else {
            loop = setInterval(_slideFrames, timePerSlide);
        }
        running = !running;
        console.log(running);
    });
    
});

$(window).resize(function () {
    setHeight();
    numberFloat();
})